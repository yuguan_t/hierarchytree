<?php

namespace App\Http\Controllers\powerOf3;

use App\Http\Controllers\Controller;

class TestController extends Controller
{
    
	public function home(){
		
		$list_of_power3 = $this->listOf3(10);
		$cur_lv = $this->getCurLevel(1093);	
		$cur_pos = $this->getCurPosition(9844);
		$parent_v = $this->getDaddy(22);
		$val = $this->getValueByPosLvl(3 , 1);
		$sons = $this->getAllSon(1,5);

		dd($sons);

	}

	/*
		Get range of the power
	*/
	private function listOf3($level){
		$power = 3;
		
		$arr = [];
		$old = 0;
		$loop = 0;

		while($loop <= $level){ 
			
			$power_of_value = pow($power , $loop) + $old;

			$arr[$loop]['start'] = $old + 1;
			$arr[$loop]['end'] = $power_of_value ;
			
			$old = $power_of_value;
			$loop++;
		}//end while
		
		return $arr;
	}//end of function
	




	/*
		Get the Number Of Power
	*/
	private function getCurLevel($value){
		$power = 3;
		$stack_of_value = 0;
		$loop = 0;

		while($stack_of_value < $value){
			$stack_of_value += pow($power , $loop);
			$loop++;
		}

		return $loop-1;
	}//end of fucnction
	





	/*
		Get Current Position Index (Start From 0)
	*/
	private function getCurPosition($value){
		//get Level
		$power = 3;
		$lv = $this->getCurLevel($value);

		$loop = 0;
		$stack_of_value = 0;

		while( $loop <= ($lv-1)  ){
			$stack_of_value += pow($power , $loop);
			$loop++;
		}

		$pos = $value - $stack_of_value;
		return $pos;
	}
	




	/*
		Get parent

	*/
	private function getDaddy($value , $arr = []){
		$power = 3;
		$lv = $this->getCurLevel($value);
		$pos = $this->getCurPosition($value);
		
		$left_v = $pos -1;
		$upper_size = $left_v / 3;
		
		if ( strpos( $upper_size, '.' ) === false ){
			$pos_daddy = $upper_size + 1;
		}else{
			$pos_daddy = ceil($upper_size);
		}
			
		$val_daddy = $this->getValueByPosLvl($pos_daddy , $lv-1);
		$arr[] = $val_daddy;
		
		if($lv != 1){
			$arr = $this->getDaddy($val_daddy , $arr);
		}
	
		return $arr;
	}
	




	/*
		Sub Function For getAllSon
	*/
	private function getSon($value){
		$power = 3;
		$lv = $this->getCurLevel($value);
		$pos = $this->getCurPosition($value);

		$left_v = $pos -1;
		$left_size = $left_v * 3;

		$next_lv = $lv + 1;
		$gen[] = $this->getValueByPosLvl($left_size + 1 , $next_lv);
		$gen[] = $this->getValueByPosLvl($left_size + 2 , $next_lv);
		$gen[] = $this->getValueByPosLvl($left_size + 3 , $next_lv);
		
		return $gen;

	}
	





	/*
		Get list of Son based on generation
	*/
	private function getAllSon($value , $generation){

		$arr[] = $value;
		$son = [];
		$loop = 0;
		$son_arr = [];

		while($generation > $loop){
			foreach($arr as $k => $d){ //get son
				$son[] = $this->getSon($d);
			}
			$merged_son = array_unique(call_user_func_array('array_merge', $son));
			$arr = $merged_son;

			$son_arr += $arr ;
			$loop++;
		}
		return $son_arr;
	}






	/*
		Get Value By Postion and Level
	*/
	private function getValueByPosLvl($pos , $lvl){
		$power = 3;

		$loop = 0;
		$value_stack = 0;

		while($loop < $lvl){
			$value_stack += pow($power , $loop);
			$loop++;
		}

		$value = $value_stack + $pos;
		return $value;
	}






}//end of class

